import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ActivityIndicator,
  FlatList,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useState} from 'react';
import {useEffect} from 'react';


const App = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getMovies = async () => {
    try {
      const response = await fetch(
        'https://tinyfac.es/api/data?limit=50&quality=0',
      );
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getMovies();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Icon
          name="chevron-left"
          size={20}
          color="#ff6876"
          style={{marginLeft: 10, marginTop: 5}}
        />
        <Text style={styles.font1}>Back</Text>
        <Text style={styles.font2}>Super Like me</Text>
        <Text style={styles.font3}>SELECT</Text>
      </View>

      <View style={styles.cont}>
        <TextInput style={styles.input1} placeholder="search" />
        <Icon
          name="search"
          size={20}
          color="#eff0f4"
          style={{marginLeft: 10, position: 'absolute', top: 20, left: '32%'}}
        />
      </View>
      <View style={styles.cont1}>
        <Text style={styles.font4}>All matches</Text>
        <Icon
            name="exclamation-circle"
            size={20}
            color="#fe6b55"
            style={{marginLeft: 10,marginRight:20,}}
          />
       
          <Text style={styles.hh}>1h</Text>
          <Icon name="list" size={20} color="#364451" style={{}} />
          <Icon
            name="th-large"
            size={20}
            color="#364451"
            style={{marginLeft: 10,marginRight:20,}}
          />
        
      </View>

      <View style={{marginTop: 20, backgroundColor: '#eff0f4'}}>
        {isLoading ? (
          <ActivityIndicator />
        ) : (
          <FlatList
            data={data}
            keyExtractor={({id}, index) => id}
            
            renderItem={({item}) => (
              <View style={styles.fcont}>
               
                  <Image
                    source={{uri: item.url}}
                    style={{
                      width: 50,
                      height: 50,
                      borderWidth: 4,
                      borderRadius: 30,
                      borderColor: '#fff',
                      flexDirection:'row',

                      margin: 8,
                      marginLeft: 20,
                    }}></Image>
                    <Icon
                    name="circle"
                    size={15}
                    color="#1ede57"
                    style={{position:'absolute',left:65,top:50,borderWidth:2,borderColor:'#fff',borderRadius:50,}}
                  />
                   <View style={styles.func}>
                  <Text style={styles.flat}>{item.first_name}</Text>
                  

                  <Icon
                    name="map-marker"
                    size={20}
                    color="#fe6b55"
                    style={{}}
                  />
                  <Text style={styles.flat1}>{item.gender}</Text>
                  </View>
                  <Icon
                    name="mars-stroke"
                    size={20}
                    color="#fe6b55"
                    style={{marginBottom:30,marginLeft:5,}}
                  />
                  <Icon
                    name="star"
                    size={20}
                    color="skyblue"
                    style={{marginLeft:'auto', marginTop: -40}}
                  />
               
              </View>
            )}
          />
        )}
      </View>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  top: {
    flexDirection: 'row',
    marginTop: 10,
  },
  font1: {
    color: '#ff6876',
    fontSize: 20,
    marginLeft: 10,
  },
  font2: {
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 30,
  },
  font3: {
    color: '#ff6876',
    fontSize: 20,
    marginLeft: 'auto',
    marginRight: 10,
  },
  cont: {
    backgroundColor: '#eff0f4',
    marginTop: 20,
  },
  input1: {
    borderStyle: 'solid',
    borderColor: 'white',
    backgroundColor: 'white',
    height: 40,
    width: '85%',
    margin: 12,
    borderWidth: 1,
    padding: 10,
    textAlign: 'center',
    fontSize: 20,
    marginLeft: 25,
  },
  cont1: {
    flexDirection: 'row',
    marginTop: 20,
    backgroundColor: '#f7f8fb',
    borderBottomWidth:3,
    paddingBottom:10,
    borderBottomColor:'#eff0f4',
    width:'90%',
    marginLeft:20,
    
  },
  font4: {
    color: 'black',
    marginLeft: 10,
    backgroundColor: '#fff',

  },
  icons1: {
    marginLeft: 'auto',
    flexDirection: 'row',
    marginRight: 30,
  },
  hh: {
    color: '#364451',
    marginRight: 10,
    marginLeft:'auto',
  },
 
  
  fcont: {
    height: 80,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 9},
    shadowOpacity: 30,
    shadowRadius: 12,
    elevation: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 16,
    paddingRight: 14,
    marginTop: 6,
    marginBottom: 6,
    marginLeft: 16,
    marginRight: 16,
    flexDirection: 'row',
    marginTop:10,
  },
  flat_t: {
    backgroundColor: '#ffffff',
    width: '100%',
    
  },
  flat1:{
    marginTop:-20,
    marginLeft:15,
    
  },
  flat:{
    color:'black',
    fontSize:20,
    marginTop:10,
    fontWeight:'bold'
    
  },
  func:{
    flexDirection:'column',
    marginBottom:20,
  },
  
});
